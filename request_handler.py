import client
import request_handler_ok
import request_handler_error
def validation(url):
	t = client.check(url)
	
	if (t == 200):
		request_handler_ok.send(t)

	else:
		request_handler_error.send(t)
