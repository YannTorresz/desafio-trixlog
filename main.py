import schedule
import time
import request_handler
import config

def job():
    request_handler.validationconfig.url(config.url)
    

schedule.every(10).seconds.do(job)


while True:
    schedule.run_pending()
    time.sleep(1)