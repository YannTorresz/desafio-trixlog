from pymongo import MongoClient

DB = MongoClient('localhost', 27017)
repository = DB.request_log_repository
log = repository.request_log

def request():
	results = log.find().limit(1).sort('request_date', -1)
	for result in results:
		result = str(result)
	result = result.split("'")[3]
	return result


def save(status, request_log):
	if (status != request_log.status):	
		obj = {'status': request_log.status, 'request_date': request_log.request_date}
		log.insert(obj)