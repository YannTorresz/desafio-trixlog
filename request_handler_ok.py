import config
import datetime
import request_log_repository
from request_log import Request_log

def send(status_code):
	status = request_log_repository.request()
	if (status != "Online"):
		config.bot.sendMessage(config.chatId, "O sistema esta online \nHTTP Status Code: {}".format(status_code))

	request_log = Request_log("Online", datetime.datetime.now())
	request_log_repository.save(status, request_log)